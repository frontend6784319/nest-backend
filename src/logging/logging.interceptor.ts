import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { Request, Response } from 'express';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private readonly reflector: Reflector) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request: Request = context.switchToHttp().getRequest();
    const response: Response = context.switchToHttp().getResponse();
    const { host } = request.headers;
    const methodType = request.method;
    const url = request.originalUrl;

    const now = Date.now();
    return next.handle().pipe(
      tap(() => {
        Logger.log(
          `${methodType} ${url} ${Date.now() - now}ms response: ${response} host:${host}`,
          context.getClass().name,
        );
      }),
    );
  }
}
