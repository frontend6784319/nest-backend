import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SharedService } from './shared/shared.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, SharedService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const thisYear = new Date().getFullYear();
      const month = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      const thisMonth = month[new Date().getMonth()];
      const today =
        new Date().getDate() > 9
          ? new Date().getDate()
          : '0' + new Date().getDate();
      const thisHour =
        new Date().getHours() > 9
          ? new Date().getHours()
          : '0' + new Date().getHours();
      const thisMinute =
        new Date().getMinutes() > 9
          ? new Date().getMinutes()
          : '0' + new Date().getMinutes();
      const thisSecond =
        new Date().getSeconds() > 9
          ? new Date().getSeconds()
          : '0' + new Date().getSeconds();

      expect(appController.getHello()).toBe(
        `Hello World! ${thisYear}-${thisMonth}-${today}:${thisHour}:${thisMinute}:${thisSecond}`,
      );
    });
  });
});
