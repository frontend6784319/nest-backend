import { Injectable } from '@nestjs/common';
import { SharedService } from './shared/shared.service';
@Injectable()
export class AppService {
  constructor(private readonly sharedService: SharedService) {}

  getHello(): string {
    return `Hello World! ${this.sharedService.getNow()}`;
  }
}
