import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'EUGENE_DATABASE_CONNECTION',
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(process.env.MONGODB_URL_EUGENE),
  },
];
