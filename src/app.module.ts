import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { LoggerMiddleware } from './common/middleware/logger.middleware';
import { ExpenseModule } from './expense/expense.module';
import { ProjectsModule } from './projects/projects.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { SharedModule } from './shared/shared.module';
import { SharedService } from './shared/shared.service';

@Module({
  imports: [
    ConfigModule.forRoot({ cache: true }),
    CatsModule,
    ExpenseModule,
    ProjectsModule,
    AuthModule,
    UsersModule,
    SharedModule,
  ],
  controllers: [AppController],
  providers: [AppService, SharedService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // Classes middleware 的寫法
    consumer.apply(LoggerMiddleware).forRoutes('*');
    // Functional middleware 的寫法
    // consumer.apply(logger).forRoutes(CatsController);
  }
}
