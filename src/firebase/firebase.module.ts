import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as admin from 'firebase-admin';
import { FirebaseRepository } from './firebase.repository';

const firebaseProvider = {
  provide: 'FIREBASE_APP',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const firebaseConfig = {
      type: configService.get<string>('EXPENSE_TRACKER_TYPE'),
      project_id: configService.get<string>('EXPENSE_TRACKER_PROJECT_ID'),
      private_key_id: configService.get<string>(
        'EXPENSE_TRACKER_PRIVATE_KEY_ID',
      ),
      private_key: configService.get<string>('EXPENSE_TRACKER_PRIVATE_KEY'),
      client_email: configService.get<string>('EXPENSE_TRACKER_CLIENT_EMAIL'),
      client_id: configService.get<string>('EXPENSE_TRACKER_CLIENT_ID'),
      auth_uri: configService.get<string>('EXPENSE_TRACKER_AUTH_URI'),
      token_uri: configService.get<string>('EXPENSE_TRACKER_TOKEN_URI'),
      auth_provider_x509_cert_url: configService.get<string>(
        'EXPENSE_TRACKER_AUTH_CERT_URL',
      ),
      client_x509_cert_url: configService.get<string>(
        'EXPENSE_TRACKER_CLIENT_CERT_URL',
      ),
      universe_domain: configService.get<string>(
        'EXPENSE_TRACKER_UNIVERSAL_DOMAIN',
      ),
    } as admin.ServiceAccount;

    return admin.initializeApp({
      credential: admin.credential.cert(firebaseConfig),
      databaseURL: `https://${configService.get<string>('EXPENSE_TRACKER_PROJECT_ID')}.firebaseio.com`,
      storageBucket: `${configService.get<string>('EXPENSE_TRACKER_PROJECT_ID')}.appspot.com`,
    });
  },
};

@Module({
  imports: [ConfigModule],
  providers: [firebaseProvider, FirebaseRepository],
  exports: [FirebaseRepository],
})
export class FirebaseModule {}
