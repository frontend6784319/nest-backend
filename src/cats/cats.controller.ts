import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  HttpCode,
  Param,
  Body,
  HttpStatus,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CreateCatDto } from './dto/create-cat.dto';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';

@ApiTags('cats')
@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Post()
  @HttpCode(201)
  create(@Req() request: Request, @Res() res: Response) {
    res.status(HttpStatus.CREATED).json({ msg: 'create success' });
  }

  @Get()
  findAll(@Res() res: Response) {
    res.status(HttpStatus.OK).json(['test']);
  }

  @Get(':id')
  findOne(@Param('id') id: string): string {
    console.log(id);
    return `This action returns a #${id} cat`;
  }

  // Asynchronicity
  @Get()
  async findAllByAsync(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Post()
  async createByDto(@Body() createCatDto: CreateCatDto) {
    this.catsService.create(createCatDto);
    return 'This action adds a new cat';
  }
}
