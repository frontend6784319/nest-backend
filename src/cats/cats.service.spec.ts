// my.service.spec.ts
import { Test, TestingModule } from '@nestjs/testing';
import { CatsService } from './cats.service';

describe('CatsService', () => {
  let catsService: CatsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [CatsService],
    }).compile();

    catsService = moduleRef.get<CatsService>(CatsService);
  });

  describe('create', () => {
    it('should return an array of cat', async () => {
      const result = [{ name: 'cat test', age: 1, breed: 'meow' }];

      // Add your assertions here to validate the result
      expect(
        await catsService.create({ name: 'cat test', age: 1, breed: 'meow' }),
      );
      expect(await catsService.findAll()).toStrictEqual(result);
    });
  });

  describe('findAll', () => {
    it('should return an array of cat', async () => {
      const result = [];

      // Add your assertions here to validate the result
      expect(await catsService.findAll()).toStrictEqual(result);
    });
  });
});
