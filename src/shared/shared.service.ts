import { Injectable } from '@nestjs/common';

// Mock Data
import * as twentyOne from '../mock/2021.json';
import * as twentyTwo from '../mock/2022.json';
import * as twentyThree from '../mock/2023.json';

@Injectable()
export class SharedService {
  getLocalData(): any[] {
    return [...twentyOne, ...twentyTwo, ...twentyThree];
  }

  getNow(): string {
    const thisYear = new Date().getFullYear();
    const month = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    const thisMonth = month[new Date().getMonth()];
    const today =
      new Date().getDate() > 9
        ? new Date().getDate()
        : '0' + new Date().getDate();
    const thisHour =
      new Date().getHours() > 9
        ? new Date().getHours()
        : '0' + new Date().getHours();
    const thisMinute =
      new Date().getMinutes() > 9
        ? new Date().getMinutes()
        : '0' + new Date().getMinutes();
    const thisSecond =
      new Date().getSeconds() > 9
        ? new Date().getSeconds()
        : '0' + new Date().getSeconds();

    return `${thisYear}-${thisMonth}-${today}:${thisHour}:${thisMinute}:${thisSecond}`;
  }
}
