import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

// Classes middleware 的寫法
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    // console.log('Request...', req);
    // console.log('Response...', res);
    next();
  }
}

// Functional middleware 的寫法
// export function logger(req: Request, res: Response, next: NextFunction) {
//   console.log(`Request...`);
//   console.log('Response...', res);
//   next();
// }
