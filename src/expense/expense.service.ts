import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { getFirestore } from 'firebase-admin/firestore';
import { Expense } from './interfaces/expense.Interface';
import { SharedService } from '../shared/shared.service';

function convertDateStringToTimestamp(dateStr) {
  // Split the date string into components
  const [day, monthName, year] = dateStr.split('-');

  // Map month names to numbers
  const monthMap = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11,
  };

  const hours = Math.floor(Math.random() * 24);
  const minutes = Math.floor(Math.random() * 60);
  const seconds = Math.floor(Math.random() * 60);

  // Get the month number from the map
  const month = monthMap[monthName];

  // Create a Date object with dynamic time
  const date = new Date(year, month, day, hours, minutes, seconds);

  // Return the timestamp (milliseconds since the epoch)
  return date.getTime();
}

@Injectable()
export class ExpenseService {
  constructor(
    @Inject('EXPENSE_MODEL') private readonly expenseModel: Model<Expense>,
    private readonly sharedService: SharedService,
  ) {}

  async create(createExpenseDto: CreateExpenseDto) {
    const db = getFirestore();
    await db
      .collection('demo')
      .doc(`${new Date().getTime()}_${Math.abs(Math.random() * 100000)}`)
      .set(createExpenseDto);

    return 'This action adds a new expense';
  }

  findAll() {
    return `This action returns all expense`;
  }

  findOne(id: number) {
    return `This action returns a #${id} expense`;
  }

  update(id: number, updateExpenseDto: UpdateExpenseDto) {
    console.log('updateExpenseDto => ', updateExpenseDto);
    return `This action updates a #${id} expense`;
  }

  remove(id: number) {
    return `This action removes a #${id} expense`;
  }

  async extractTransformLoad() {
    const db = getFirestore();
    const result = await db.collection('Account2024').get();
    const randomNumber: string = `${Math.random() * 100000}`;
    const nowString = this.sharedService.getNow();

    const expenseData = [];
    result.forEach((doc) => {
      expenseData.push(doc.data());
    });

    let arraydata = this.sharedService.getLocalData();
    arraydata = [...arraydata, ...expenseData];

    await db
      .collection('demo')
      .doc(`${nowString}_${Number.parseFloat(randomNumber).toFixed()}`)
      .set({ DATE: nowString, TOTAL: arraydata.length });

    arraydata.forEach(async (element) => {
      const { category, date, item, price, pay, type } = element;
      const createdProject = await this.expenseModel.create({
        account: pay,
        category: category,
        date: date,
        item: item,
        paymentType: 'Cash',
        price: price,
        type: type,
        timestamp: convertDateStringToTimestamp(date),
      });

      console.log('createdProject => ', JSON.stringify(createdProject));
    });

    return `This action extract, transform, load expense`;
  }
}
