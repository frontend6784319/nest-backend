export class CreateExpenseDto {
  readonly account: string;
  readonly category: string;
  readonly date: string;
  readonly item: string;
  readonly paymentType: string;
  readonly price: string;
  readonly type: string;
  readonly timestamp: number;
}
