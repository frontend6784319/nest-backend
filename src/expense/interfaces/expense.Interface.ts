import { Document } from 'mongoose';
import { CreateExpenseDto } from '../dto/create-expense.dto';

export interface ExpenseDataInterface {
  id: string;
  data: CreateExpenseDto;
}

export interface Expense extends Document {
  readonly account: string;
  readonly category: string;
  readonly date: string;
  readonly item: string;
  readonly paymentType: string;
  readonly price: string;
  readonly type: string;
  readonly timestamp: number;
}
