import { Module } from '@nestjs/common';
import { FirebaseModule } from '../firebase/firebase.module';
import { DatabaseModule } from '../database/database.module';
import { ExpenseService } from './expense.service';
import { ExpenseController } from './expense.controller';
import { expenseProviders } from './expense.providers';
import { SharedModule } from '../shared/shared.module';

@Module({
  imports: [FirebaseModule, DatabaseModule, SharedModule],
  controllers: [ExpenseController],
  providers: [ExpenseService, ...expenseProviders],
})
export class ExpenseModule {}
