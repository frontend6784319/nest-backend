import * as mongoose from 'mongoose';

export const ExpenseSchema = new mongoose.Schema({
  account: String,
  category: String,
  date: String,
  item: String,
  paymentType: String,
  price: String,
  type: String,
  timestamp: Number,
});
