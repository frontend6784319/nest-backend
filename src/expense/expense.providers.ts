import { Mongoose } from 'mongoose';
import { ExpenseSchema } from './schemas/expense.schema';

const thisYear = new Date().getFullYear();
const thisMonth = new Date().getMonth() + 1;
const today =
  new Date().getDate() > 9 ? new Date().getDate() : '0' + new Date().getDate();
const thisHour =
  new Date().getHours() > 9
    ? new Date().getHours()
    : '0' + new Date().getHours();
const thisMinute =
  new Date().getMinutes() > 9
    ? new Date().getMinutes()
    : '0' + new Date().getMinutes();
const thisSecond =
  new Date().getSeconds() > 9
    ? new Date().getSeconds()
    : '0' + new Date().getSeconds();

export const expenseProviders = [
  {
    provide: 'EXPENSE_MODEL',
    useFactory: (mongoose: Mongoose) =>
      mongoose.model(
        `${thisYear}-${thisMonth}-${today}:${thisHour}:${thisMinute}:${thisSecond}`,
        ExpenseSchema,
      ),
    inject: ['EUGENE_DATABASE_CONNECTION'],
  },
];
